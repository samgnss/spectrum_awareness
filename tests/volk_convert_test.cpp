#include <complex>
#include <iostream>
#include <vector>
#include <volk/volk.h>
#include <unistd.h>
#include <chrono>

int main()
{
    std::vector<int8_t> in(10000);
    int8_t *inp = (int8_t *)volk_malloc(10000, volk_get_alignment());

    for (int i = 0; i < in.size(); i++)
    {
        int tmp = rand() % 127;
        in[i] = tmp;
        inp[i] = tmp;
    }

    std::vector<std::complex<float>> out(5000);
    std::complex<float> *outp = (std::complex<float> *)volk_malloc(5000 * sizeof(std::complex<float>), volk_get_alignment());

    float scale = 1.0 / 128.0;

    auto start = std::chrono::steady_clock::now();
    volk_8i_s32f_convert_32f(reinterpret_cast<float *>(out.data()), in.data(), 1.0, in.size());
    auto end = std::chrono::steady_clock::now();
    auto diff = (end - start);

    auto start2 = std::chrono::steady_clock::now();
    for (int i = 0, j = 0; i < in.size(); i += 2, j++)
    {
        auto tmp = in[i] - 255;
        auto tmp2 = in[i + 1] - 255;
        out[j] = scale * std::complex<float>(tmp, tmp2);
    }
    auto end2 = std::chrono::steady_clock::now();
    auto diff2 = (end2 - start2);

    auto start3 = std::chrono::steady_clock::now();
    volk_8i_s32f_convert_32f(reinterpret_cast<float *>(outp), in.data(), 1.0, in.size());
    auto end3 = std::chrono::steady_clock::now();
    auto diff3 = (end3 - start3);

    auto start4 = std::chrono::steady_clock::now();
    volk_8i_s32f_convert_32f(reinterpret_cast<float *>(outp), inp, 1.0, in.size() - 1);
    auto end4 = std::chrono::steady_clock::now();
    auto diff4 = (end4 - start4);

    std::cout << "Conversion took: " << std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count() << " us vs other conv: " << std::chrono::duration_cast<std::chrono::nanoseconds>(diff2).count() << std::endl;
    std::cout << "Aligned output vs unaligned input took: " << std::chrono::duration_cast<std::chrono::nanoseconds>(diff3).count() << std::endl;
    std::cout << "Aligned took: " << std::chrono::duration_cast<std::chrono::nanoseconds>(diff4).count() << std::endl;

    float *t = (float *)volk_malloc(1000 * 4, volk_get_alignment());
    for (int i = 0; i < 1000; i++)
    {
        t[i] = 1;
    }

    volk_32f_x2_add_32f(t, t, t, 1000);

    for (int i = 0; i < 1000; i++)
    {
        std::cout << t[i] << std::endl;
    }
}