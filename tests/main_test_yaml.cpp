#include <iostream>
#include <common/ConfigParser.hpp>
#include <common/common.hpp>
#include <yaml-cpp/yaml.h>

int main(){
    auto config = YAML::LoadFile("../configs/default.yaml");
    std::cout << config["RADIO"]["TUNE_RANGES"].IsSequence() << std::endl;
    for (auto it = config["RADIO"]["TUNE_RANGES"].begin(); it != config["RADIO"]["TUNE_RANGES"].end(); it++){
        std::cout << it->IsSequence() << std::endl;
    }
    

    return 0;
}