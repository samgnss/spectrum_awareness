#include <iostream>
#include <memory>
#include <vector>

#include <signal.h>

#include <boost/program_options.hpp>

#include <common/common.hpp>
#include <hardware/hackrf/HackRfIf.hpp>
#include <hardware/soapy/SoapyIf.hpp>
#include <processing/detector.hpp>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace po = boost::program_options;

int main(int argc, char **argv) {
  // variables to be set by po
  std::string configFile;

  // setup the program options
  po::options_description desc("Allowed options");
  desc.add_options()("help", "Monitor frequencies")(
      "config",
      po::value<std::string>(&configFile)
          ->default_value("./configs/default.yaml"),
      "xml configuration file");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 0;
  }

  // std::shared_ptr<databaseLogger> db =
  // std::shared_ptr<databaseLogger>(databaseLogger::getInst("log.db"));
  auto console = spdlog::stdout_color_mt("SPECTRUM");
  spec::config_t parser = std::make_shared<ConfigParser>(configFile);

  SPDLOG_LOGGER_INFO(console,"Starting radio");
  auto radio_if = std::make_unique<radio::SoapyIf>(parser, console);

  SPDLOG_LOGGER_INFO(console,"Starting signal processing");
  auto det_proc = std::make_unique<detector>(parser);

  spec::RXCallback_t callable = std::bind(&detector::process, det_proc.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

  radio_if->RegisterCallback(callable);

  radio_if->Start();

  // wait for kill signal
  SPDLOG_LOGGER_INFO(console,"Enter 0 to stop: ");
  int c = 1;
  while (c != 0) {
    std::cin >> c;
  }

  radio_if->Stop();

  return 0;
}
