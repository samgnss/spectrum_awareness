import sys
from dataclasses import dataclass
from multiprocessing import Process, Queue

import pyqtgraph as pgt
import zmq
from PyQt5 import QtCore, QtGui, QtWidgets

import common_pb2
import data_pb2
import numpy as np


class zmq_pull_server:
    def __init__(self, port, msg_queue):
        self.port = "tcp://127.0.0.1:" + str(port)
        print(self.port)
        self.queue = msg_queue
        self.running = False
        self.proc = None

    def run_server(self):
        print("Starting server")
        self.running = True
        self.proc = Process(target=self.receiver)
        self.proc.start()

    def stop_server(self):
        self.running = False
        self.proc.join()
        
    def receiver(self):
        print("Starting rx thread")
        context = zmq.Context()
        sock = context.socket(zmq.SUB)
        sock.connect(self.port)
        sock.setsockopt_string(zmq.SUBSCRIBE, "psd")
        
        while self.running:
            # print("Receiving message")
            msg = sock.recv()[len("psd")+1:]

            wrapper = common_pb2.wrapper()
            wrapper.ParseFromString(msg)

            _psd = data_pb2.fft()
            _psd.ParseFromString(wrapper.message)
            self.queue.put(_psd)

class PsdApp(QtWidgets.QMainWindow):
    def __init__(self, psd_q):
        super().__init__()
        self.setupUi()
        
        self.plot = self.plotw.plot()
        self.plot.setPen((0,100,100))
        self.plotw.showGrid(x=True, y=True)
        self._psd_q = psd_q
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(50)
        self._ylim = None
        self._xlim = None

    def setupUi(self):
        self.w = QtGui.QWidget()
        self.setCentralWidget(self.w)
        self.layout = QtGui.QVBoxLayout()
        self.w.setLayout(self.layout)

        self.plotw = pgt.PlotWidget()
        self.layout.addWidget(self.plotw)

    def updateplot(self):
        try:
            data = self._psd_q.get(block=False)
        except:
            return

        meta = data.meta
        xmin = (meta.frequency_hz - meta.sample_rate // 2)/1e6
        xmax = (meta.frequency_hz + meta.sample_rate // 2)/1e6
        fftdata = [10*np.log10(float(x)) for x in data.data]

        self.plotw.setXRange(xmin, xmax)
        self.plot.setData(y=fftdata, x=np.linspace(xmin, xmax, len(fftdata)))
        # print("plotted")

def main():
    app = QtWidgets.QApplication(sys.argv)
    queue = Queue()
    server = zmq_pull_server(50001, queue)
    server.run_server()
    window = PsdApp(queue)
    window.show()
    app.exec_()
    server.stop_server()

if __name__ == "__main__":
    main()
