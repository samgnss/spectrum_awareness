import argparse
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fname', type=str, required=True, help="File name")

    args = parser.parse_args()

    fid = open(args.fname, 'rb')

    _, ax = plt.subplots()

    time = np.fromfile(fid, np.int64, count=1)
    while len(time):
        fftsize = np.fromfile(fid, np.int32, count=1)
        thresh = np.fromfile(fid, np.float32, count=fftsize[0])
        psd = np.fromfile(fid, np.float32, count=fftsize[0])
        # psd[-2] = 0
        # psd[-1] = 0
        # psd[0] = 0
        # psd[1] = 0
        # psd[2] = 0
        ax.cla()
        ax.plot(psd)
        ax.plot(thresh)
        plt.pause(0.1)
        # plt.show()
        time = np.fromfile(fid, np.int64, count=1)
        print(time)

    plt.show()