from dataclasses import dataclass
import zmq
from multiprocessing import Process

import common_pb2
import detection_pb2

class zmq_pull_server:
    def __init__(self, port, msg_queue):
        self.port = "tcp://127.0.0.1:" + str(port)
        print(self.port)
        self.queue = msg_queue
        self.running = False
        self.proc = None

    def run_server(self):
        print("Starting server")
        self.running = True
        self.proc = Process(target=self.receiver)
        self.proc.start()

    def stop_server(self):
        self.running = False
        self.proc.join()
        
    def receiver(self):
        print("Starting rx thread")
        context = zmq.Context()
        sock = context.socket(zmq.SUB)
        sock.connect(self.port)
        sock.setsockopt_string(zmq.SUBSCRIBE, "detections")
        

        while self.running:
            # print("Receiving message")
            msg = sock.recv()[len("detections")+1:]
            # print(msg)

            wrapper = common_pb2.wrapper()
            wrapper.ParseFromString(msg)

            det = detection_pb2.detection()
            det.ParseFromString(wrapper.message)

            det_cls = detection(det.cf_hz, det.bw_hz, det.pwr_lin, det.time, det.proc_time)
            self.queue.put(det_cls)


@dataclass
class detection:
    cf: float
    bw: float
    pwr_lin: float
    time: float
    proc_time:float

    def get_freq_range(self):
        return self.cf - self.bw/2, self.cf + self.bw/2

class channel:
    def __init__(self, det: detection):
        self.start_freq, self.stop_freq = det.get_freq_range()
        self.start_time = det.time
        self.end_time = det.time
        self.pwr_lin = det.pwr_lin
        self.det_count = 1
    
    def check_overlap(self, det: detection):
        f1, f2 = det.get_freq_range()
        return (self.start_freq < f2)  and (f1 < self.stop_freq)

    def check_channel_overlap(self, chan):
        return (self.start_freq < chan.stop_freq)  and (chan.start_freq < self.stop_freq)

    def add_detection(self, det: detection):
        f1, f2 = det.get_freq_range()

        self.start_freq = min(self.start_freq, f1-60e3)
        self.stop_freq = max(self.stop_freq, f2+60e3)

        self.end_time = det.time
        self.pwr_lin += det.pwr_lin
        self.det_count += 1

    def merge_channel(self, chan):
        self.start_freq = min(self.start_freq, chan.start_freq)
        self.stop_freq = max(self.stop_freq, chan.stop_freq)

        self.start_time = min(self.start_time, chan.start_time)
        self.end_time = max(self.end_time, chan.end_time)
        self.pwr_lin = self.pwr_lin + chan.pwr_lin
        self.det_count += chan.det_count


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from matplotlib.patches import Rectangle
    from matplotlib.collections import PatchCollection
    from multiprocessing import Queue
    import numpy as np
    import time

    fig, ax = plt.subplots(1)
    # fig2, ax2 = plt.subplots(1)

    queue = Queue()
    server = zmq_pull_server(50000, queue)

    server.run_server()

    channels = []
    times = []
    pwrs = []
    cfs = []
    proc_time = []

    while server.running:
        for i in range(100):
            try:
                data = queue.get(block=False)
            except:
                time.sleep(1)
                break

            times.append(data.time/1e9)
            pwrs.append(data.pwr_lin)
            cfs.append(data.cf)
            proc_time = data.proc_time


        # if not channels:
        #     channels.append(channel(data))
        # else:
        #     added = False
        #     for chan in channels:
        #         if chan.check_overlap(data):
        #             chan.add_detection(data)
        #             added = True
        #             break

        #     if not added:
        #         channels.append(channel(data))

        # merged_channels = set()
        # for idx1, chan1 in enumerate(channels):
        #     for idx2, chan2 in enumerate(channels):
        #         if idx1 == idx2 or idx1 in merged_channels or idx2 in merged_channels:
        #             continue

        #         if chan2.check_channel_overlap(chan1):
        #             chan2.merge_channel(chan1)
        #             merged_channels.add(idx1)
                    
        
        # channels = [chan for idx, chan in enumerate(channels) if idx not in merged_channels]

        ax.scatter(np.array(times), cfs, c=20*np.log10(pwrs))
        # ax2.plot(np.array(proc_time), 'o')
        
        patches = []
        for chan in channels:
            if chan.det_count > 20:
                patches.append(Rectangle((chan.start_time, chan.start_freq), (chan.end_time - chan.start_time), chan.stop_freq - chan.start_freq))

        collec = PatchCollection(patches,facecolor='y', alpha=0.5)
        ax.add_collection(collec)
                

        plt.pause(0.000001)
        ax.cla()
