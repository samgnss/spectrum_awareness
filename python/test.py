import numpy as np
import time
import matplotlib.pyplot as plt
from numpy.lib import stride_tricks


def get_strides(array_size, fft_size, noverlap):
    shift = fft_size - noverlap
    idx = 0
    vals = []
    while idx < array_size:
        vals.append(idx)
        idx += shift
    return array_size - idx + shift, vals


arr = np.tile(np.arange(1024), (10))
idxs = get_strides(len(arr), 1024, 512)

cols = np.ceil((len(arr) - 1024) / float(512)) + 1
print(cols, len(idxs[1]))


frames = stride_tricks.as_strided(
        arr,
        shape=(len(idxs[1]), 1024),
        strides=(arr.strides[0] * 512, arr.strides[0])).copy()

print(frames)

arr2 = []
for idx in idxs[1]:
    arr2.append(arr[idx:(idx+1024)])
print(len(arr2[-1]))
arr2 = np.array(arr2[:-1])
print(arr2)
plt.figure()
plt.imshow(arr2, aspect='auto')

plt.figure()
plt.imshow(frames, aspect='auto')
plt.show()
