/*
 * FFT object
 */

#ifndef __fft__
#define __fft__

#include <complex>
#include <fftw3.h>
#include <string>
#include <volk/volk.h>
#include <common/common.hpp>

class FFT
{
public:
    FFT(int fftSize, const std::string windowType);
    ~FFT();

    void resetFFTSize(int newSize, int inputSize);
    void getFFT(spec::fc32_t *input, spec::fc32_t *output) const;
    void getFFT_ABS(spec::fc32_t *input, float *output) const;
    void getIFFT(spec::fc32_t *input, spec::fc32_t *output);
    void setWindow(std::string windowType, int windowSize);

    unsigned int fftSize;

private:
    fftwf_complex *outputMem;
    fftwf_complex *inputMem;
    spec::fc32_t *window;
    fftwf_plan forwardDFT;
    fftwf_plan inverseDFT;
};

#endif
