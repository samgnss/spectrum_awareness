#include <vector>

#include <signal_processing/spec_est/psd.hpp>
#include <signal_processing/signal_utils.hpp>

void welches_psd(spec::AlignedVector<float> &psd, spec::AlignedVector<spec::fc32_t> &iq, const std::shared_ptr<FFT> fftp, float overlap_factor)
{
    // get windowed strides
    // do the fft on each window
    // average

    std::vector<std::size_t> idxs;
    auto num_zero_pad = spec_dsp::get_strides(idxs, iq.size(), fftp->fftSize, (float)fftp->fftSize * overlap_factor);

    if (num_zero_pad)
    {
        spec::AlignedVector<spec::fc32_t> zeros(num_zero_pad);
        iq.insert(iq.end(), zeros.begin(), zeros.end());
    }

    if (psd.size() < fftp->fftSize)
    {
        psd.resize(fftp->fftSize);
    }

    spec::AlignedVector<float> fft_scratch(fftp->fftSize);
    for (const auto &idx : idxs)
    {
        fftp->getFFT_ABS(&iq[idx], &fft_scratch[0]);

        volk_32f_x2_add_32f(psd.data(), fft_scratch.data(), psd.data(), psd.size());
    }
    volk_32f_s32f_normalize(psd.data(), (float)idxs.size(), psd.size());
}