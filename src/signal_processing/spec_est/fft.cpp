#include "fft.h"

#include <volk/volk.h>

FFT::FFT(int fftSize, const std::string windowType) : fftSize(fftSize)
{
    //TODO: add FFT wisdom
    outputMem = fftwf_alloc_complex(fftSize * sizeof(fftwf_complex));
    inputMem = fftwf_alloc_complex(fftSize * sizeof(fftwf_complex));
    forwardDFT = fftwf_plan_dft_1d(fftSize, inputMem, outputMem, FFTW_FORWARD, FFTW_MEASURE);
    inverseDFT = fftwf_plan_dft_1d(fftSize, inputMem, outputMem, FFTW_BACKWARD, FFTW_MEASURE);

    //define window
    window = (spec::fc32_t *)volk_malloc(fftSize * sizeof(spec::fc32_t), volk_get_alignment());
    this->setWindow(windowType, fftSize);
};

FFT::~FFT()
{
    volk_free(window);

    //free tmp
    fftwf_free(inputMem);
    fftwf_free(outputMem);

    fftwf_destroy_plan(forwardDFT);
    fftwf_destroy_plan(inverseDFT);
};

void FFT::resetFFTSize(int fftSize, int inputSize){
    // this->fftSize = fftSize;
    // outputMem = fftwf_alloc_complex(fftSize * sizeof(fftwf_complex));
    // tmp = fftwf_alloc_complex(inputSize * sizeof(fftwf_complex));
    // forwardDFT = fftwf_plan_dft_1d(fftSize, tmp, outputMem, FFTW_FORWARD, FFTW_ESTIMATE);
    // inverseDFT = fftwf_plan_dft_1d(fftSize, tmp, outputMem, FFTW_BACKWARD, FFTW_ESTIMATE);
};

void FFT::getFFT(spec::fc32_t *input, spec::fc32_t *output) const
{
    volk_32fc_x2_multiply_32fc(reinterpret_cast<spec::fc32_t *>(inputMem), window, input, fftSize);
    fftwf_execute(forwardDFT);
    volk_32fc_s32fc_multiply_32fc(output, reinterpret_cast<spec::fc32_t *>(outputMem), 1.f / (float)fftSize, fftSize);
};

void FFT::getFFT_ABS(spec::fc32_t *input, float *output) const
{
    volk_32fc_x2_multiply_32fc(reinterpret_cast<spec::fc32_t *>(inputMem), window, input, fftSize);
    fftwf_execute(forwardDFT);
    volk_32fc_s32fc_multiply_32fc(reinterpret_cast<spec::fc32_t *>(outputMem), reinterpret_cast<spec::fc32_t *>(outputMem), 1.f / (float)fftSize, fftSize);
    volk_32fc_magnitude_squared_32f(output, reinterpret_cast<spec::fc32_t *>(outputMem), fftSize);
};

void FFT::getIFFT(spec::fc32_t *input, spec::fc32_t *output)
{
    fftwf_complex *inputTMP = reinterpret_cast<fftwf_complex *>(input);
    fftwf_complex *outputTMP = reinterpret_cast<fftwf_complex *>(output);
    fftwf_execute_dft(inverseDFT, inputTMP, outputTMP);
};

void FFT::setWindow(std::string windowType, int windowSize)
{
    //Hanning window
    if (windowType == "hanning")
    {
        for (int i = 0; i < windowSize; ++i)
        {
            window[i] = 0.5f * (1.f - (float)std::cos(2 * M_PI * i / (fftSize - 1)));
        }
    }
    else if (windowType == "blackman")
    {
        for (int i = 0; i < windowSize; ++i)
        {
            window[i] = 0.42 - 0.5 * std::cos(2 * M_PI * i / (fftSize - 1)) + 0.08 * std::cos(4 * M_PI * i / (fftSize - 1));
        }
    }
    else if (windowType == "rect")
    {
        for (int i = 0; i < windowSize; ++i)
        {
            window[i] = 1;
        }
        }
};
