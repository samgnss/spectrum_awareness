#pragma once
#include <common/common.hpp>
#include <signal_processing/spec_est/fft.h>

void welches_psd(spec::AlignedVector<float> &psd, spec::AlignedVector<spec::fc32_t> &iq, const std::shared_ptr<FFT> fftp, float overlap_factor = 0.5);
