#pragma once
#include <common/common.hpp>

namespace spec_dsp
{
constexpr float int_to_float_scale = 1.0 / 128.0;
inline void uint8_to_fc32(spec::fc32_t *out, const uint8_t *inp, const std::size_t num_samples)
{
    for (std::size_t i = 0, j = 0; i < num_samples; i += 2, j++)
    {
        auto tmp = inp[i] - 255;
        auto tmp2 = inp[i + 1] - 255;
        out[j] = int_to_float_scale * std::complex<float>(tmp, tmp2);
    }
};

inline int get_strides(std::vector<std::size_t> &row_idxs, const std::size_t array_size, const int fft_size, const int noverlap)
{
    assert(fft_size > noverlap);
    int shift = fft_size - noverlap;

    std::size_t idx = 0;
    while (idx < array_size)
    {
        row_idxs.push_back(idx);
        idx += shift;
    }

    return array_size - idx + shift;
};

template <typename T>
void blanker(spec::AlignedVector<T> &data, const std::vector<std::size_t> &idxs)
{
    for (const auto &idx : idxs)
    {
        data[idx] = (T)0;
    }
}

template <typename T>
void fft_shift(spec::AlignedVector<T> &fft)
{
    auto dc = fft.size() >> 1;
    fft.insert(fft.begin(), fft.begin() + dc, fft.end());
    fft.erase(fft.begin() + (dc << 1), fft.end());
}

}; // namespace spec_dsp