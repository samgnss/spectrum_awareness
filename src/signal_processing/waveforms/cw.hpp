#pragma once

#include <common/common.hpp>
#include <math>
#include <volk/volk.hpp>

namespace wfrm{
    class CW{
        public:
            constexpr static char desc[] = "CW";

            CW(double frequency, double sample_rate, double dur);
            ~CW() = default;

            void add_cw(double frequency);
            
            const spec::fc32_t* get_ptr(sample_offset=0) const;
            const spec::AlignedVector<spec::fc32_t>& get_ref(sample_offset=0) const;

        private:
            double m_frequency;
            double m_sample_rate;
            int m_num_samples;
            spec::AlignedVector<spec::fc32_t> m_buffer;
    };

    CW::CW(double frequency, double sample_rate, double dur):m_frequency(frequency), m_sample_rate(sample_rate){
        m_num_samples = std::static_cast<int>(m_sample_rate*dur);
        m_buffer = spec::AlignedVector<spec::fc32_t>(num_samples);
        // I think the volk rotators are still broken...
        for(int i = 0; i < m_num_samples; ++i){
            m_buffer[i] = std::polar(1, 2*M_PI*m_frequency*i/m_sample_rate);
        }
    }

    void add_cw(double frequency){
        for(int i = 0; i < m_num_samples; ++i){
            m_buffer[i] += std::polar(1, 2*M_PI*m_frequency*i/m_sample_rate);
        }
        volk_32f_s32f_normalize(std::reinterpret_cast<float>(m_buffer.data()), 2.0, 2*m_num_samples);
    };

    const spec::fc32_t* get_ptr(sample_offset=0) const{
        return m_buffer[sample_offset].data();
    }

    const spec::AlignedVector<spec::fc32_t>& get_ref(sample_offset=0) const{
        return std::ref(m_buffer[sample_offset]);
    }


}