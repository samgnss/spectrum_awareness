#pragma once

#include <fstream>
#include <memory>
#include <vector>

#include <common/common.hpp>

using detectionsWithThresh =
    std::pair<std::vector<spec::detection>, spec::AlignedVector<float>>;

class cfar
{
public:
    cfar(float pfa, int numCells, int numGuardBins, int buffLen, int freqSlip,
         int sampleRate);
    ~cfar();
    detectionsWithThresh getDetections(const spec::AlignedVector<float> &fftMagIn);
    spec::AlignedVector<spec::detection> getDetectionsNoThresh(const spec::AlignedVector<float> &fftMagIn);
    spec::AlignedVector<float> getDetectionThreshold(const spec::AlignedVector<float> &fftMagIn);

private:
    spec::AlignedVector<spec::detection>
    compaction(const spec::AlignedVector<spec::detection> &dets);
    spec::detection newTarget(const spec::detection &det);
    spec::detection addDetToTarget(spec::detection &target,
                                   const spec::detection &det);

    void InitTrainingCells(const float *input);
    void InitTrainingCells(const spec::AlignedVector<float> &input);

    float mAlpha;
    float mFreqResolution;
    int mNumCells;
    int mNumGuardBins;
    int mBuffLen;
    int mFreqSlip;
    int mFreqSlipHz;
    int mSampleRate;

    float mForwardNoiseEstimate;
    float mReverseNoiseEstimate;

    size_t mForwardStart, mForwardEnd;
    size_t mReverseStart, mReverseEnd;

    spec::AlignedVector<int> shiftedBins;

    std::ofstream dbgFile;
};
