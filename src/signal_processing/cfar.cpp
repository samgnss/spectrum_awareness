#include "cfar.h"

#include <chrono>
#include <cstring>
#include <iostream>
#include <numeric>
#include <volk/volk.h>

cfar::cfar(float pfa, int numCells, int numGuardBins, int buffLen, int freqSlip,
           int sampleRate)
    : mNumCells(numCells), mNumGuardBins(numGuardBins), mBuffLen(buffLen),
      mFreqSlip(freqSlip), mSampleRate(sampleRate)
{
    mFreqResolution = (mSampleRate / mBuffLen);
    mFreqSlipHz = mFreqSlip * mFreqResolution;

    shiftedBins.resize(mBuffLen);
    std::iota(shiftedBins.begin(), shiftedBins.end(), -(mBuffLen >> 2));

    // calculate alpha
    mAlpha = numCells * (std::pow(pfa, -1.0 / numCells) - 1.0);

    dbgFile.open("cfar_noise_estimate.bin", std::ios::binary);
}

cfar::~cfar() { dbgFile.close(); };

void cfar::InitTrainingCells(const float *input)
{
    mForwardNoiseEstimate = 0;
    mForwardStart = mNumGuardBins;
    mForwardEnd = mNumCells + mNumGuardBins - 1;
    mReverseNoiseEstimate = 0;
    mReverseStart = mBuffLen - mNumCells - mNumGuardBins;
    mReverseEnd = mBuffLen - mNumGuardBins - 1;

    size_t idx1, idx2;
    for (idx1 = mForwardStart, idx2 = mReverseStart; idx1 <= mForwardEnd;
         ++idx1, ++idx2)
    {
        mForwardNoiseEstimate += input[idx1];
        mReverseNoiseEstimate += input[idx2];
    }
}

void cfar::InitTrainingCells(const spec::AlignedVector<float> &input)
{
    mForwardNoiseEstimate = 0;
    mForwardStart = mNumGuardBins;
    mForwardEnd = mNumCells + mNumGuardBins - 1;
    mReverseNoiseEstimate = 0;
    mReverseStart = mBuffLen - mNumCells - mNumGuardBins;
    mReverseEnd = mBuffLen - mNumGuardBins - 1;

    size_t idx1, idx2;
    for (idx1 = mForwardStart, idx2 = mReverseStart; idx1 <= mForwardEnd;
         ++idx1, ++idx2)
    {
        mForwardNoiseEstimate += input[idx1];
        mReverseNoiseEstimate += input[idx2];
    }
}

detectionsWithThresh cfar::getDetections(const spec::AlignedVector<float> &fftMagIn)
{
    // CFAR processing
    int bin = 0;
    int binCheck = mBuffLen >> 1;
    int detBin = 0;
    float scale = mAlpha / (2 * mNumCells);
    float mean = 0;
    float noiseEsti = 0;
    // spec::detection tmpDet;

    auto timeNow = std::chrono::high_resolution_clock::now().time_since_epoch();

    std::vector<spec::detection> dets;
    dets.reserve(mBuffLen);

    spec::AlignedVector<float> detectionThreshold(fftMagIn.size());
    double timeNowUs = std::chrono::duration_cast<std::chrono::microseconds>(timeNow).count();
    double timeNowNs = std::chrono::duration_cast<std::chrono::nanoseconds>(timeNow).count();
    auto aggTime = spec::time(timeNowUs, timeNowNs);

    InitTrainingCells(fftMagIn);

    // loop through buffer
    for (bin = 0; bin < mBuffLen; ++bin)
    {
        mean = std::abs(mForwardNoiseEstimate + mReverseNoiseEstimate);
        // compare to signal level
        noiseEsti = mean * scale;
        detectionThreshold[bin] = noiseEsti;

        // dbgFile.write((char *)&noiseEsti, sizeof(float));

        if (fftMagIn[bin] > noiseEsti)
        {
            double freqOffset = (bin - binCheck) * mFreqResolution;
            auto tmpDet = spec::detection(freqOffset, mFreqResolution, fftMagIn[bin], aggTime);

            // store detection
            dets.push_back(tmpDet);
        }

        // recalculate noise estimate
        mForwardNoiseEstimate -= fftMagIn[mForwardStart];
        mReverseNoiseEstimate -= fftMagIn[mReverseStart];

        mForwardStart = (mForwardStart + 1) % mBuffLen;
        mReverseStart = (mReverseStart + 1) % mBuffLen;

        mForwardNoiseEstimate += fftMagIn[++mForwardEnd % mBuffLen];
        mReverseNoiseEstimate += fftMagIn[++mReverseEnd % mBuffLen];
    }

    return {dets, detectionThreshold};
};

spec::AlignedVector<float>
cfar::getDetectionThreshold(const spec::AlignedVector<float> &fftMagIn)
{
    // CFAR processing
    float scale = mAlpha / (2 * mNumCells);
    float sum = 0;
    float noiseEsti = 0;

    spec::AlignedVector<float> detectionThreshold(fftMagIn.size());

    InitTrainingCells(fftMagIn);

    // loop through buffer
    for (unsigned int bin = 0; bin < fftMagIn.size(); ++bin)
    {
        // sum the training cells
        sum = std::abs(mForwardNoiseEstimate + mReverseNoiseEstimate);

        // scale the sum to get the detection threshold
        noiseEsti = sum * scale;
        detectionThreshold[bin] = noiseEsti;

        // recalculate noise estimate
        mForwardNoiseEstimate -= fftMagIn[mForwardStart];
        mReverseNoiseEstimate -= fftMagIn[mReverseStart];

        mForwardStart = (mForwardStart + 1) % mBuffLen;
        mReverseStart = (mReverseStart + 1) % mBuffLen;

        mForwardNoiseEstimate += fftMagIn[++mForwardEnd % mBuffLen];
        mReverseNoiseEstimate += fftMagIn[++mReverseEnd % mBuffLen];
    }

    return detectionThreshold;
}

// spec::AlignedVector<radar::target>
// cfar::compaction(const spec::AlignedVector<radar::cfarDet> &dets)
// {
//     // assuming no frequency slip, the maximum number of dets we can have is the
//     // same size dets
//     spec::AlignedVector<radar::target> targets;
//     targets.reserve(dets.size());

//     for (const auto &det : dets)
//     {
//         // check if the frequecny ranges overlap
//         if (targets.size() == 0)
//         {
//             targets.push_back(newTarget(det));
//         }

//         radar::target &currTarget = targets.back();

//         // check if the detection overlaps with the current target
//         uint64_t low = currTarget.freqHz.first - (currTarget.freqHz.second >> 1);
//         uint64_t high = currTarget.freqHz.first + (currTarget.freqHz.second >> 1);

//         if ((high + mFreqSlipHz) > det.freqHz && (low - mFreqSlipHz) < det.freqHz)
//         {
//             currTarget = addDetToTarget(currTarget, det);
//         }
//         else
//         {
//             targets.push_back(newTarget(det));
//         }
//     }

//     return targets;
// }

// radar::target cfar::newTarget(const radar::cfarDet &det)
// {
//     radar::target target;
//     target.freqHz.first = det.freqHz;
//     target.freqHz.second = 0;
//     target.power = det.power;
//     target.timeOnMicroSec = det.timeOnMicroSec;
//     return target;
// }

// spec::detection cfar::addDetToTarget(radar::target &target,
//                                    const radar::cfarDet &det)
// {
//     uint64_t low =
//         std::min(target.freqHz.first - (target.freqHz.second >> 1), det.freqHz);
//     uint64_t high =
//         std::max(target.freqHz.first + (target.freqHz.second >> 1), det.freqHz);

//     target.freqHz.first = (high + low) >> 1;
//     target.freqHz.second = (high - low);
//     target.power += det.power;
//     target.timeOnMicroSec = std::max(det.timeOnMicroSec, target.timeOnMicroSec);

//     return target;
// }
