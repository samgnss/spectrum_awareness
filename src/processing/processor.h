// #pragma once
// #include <atomic>
// #include <condition_variable>
// #include <deque>
// #include <fstream>
// #include <mutex>
// #include <thread>
// #include <vector>

// #include <RadarTypes.h>
// #include <ZmqPushServer.h>
// #include <cfar.h>
// #include <configParser.h>
// #include <driver/hackrf.h>
// #include <fft.h>
// #include <histogram.h>
// #include <spdlog/sinks/stdout_color_sinks.h>
// #include <volk_math.h>

// namespace hackrf {
// class processor {
// public:
//   processor(std::shared_ptr<configParser> config);
//   ~processor();
//   void Init(uint16_t startFreq, uint16_t endFreq);
//   void RxMonitor(uint8_t *rx_buff);
//   void Stop();

// private:
//   typedef std::vector<
//       std::pair<std::vector<radar::sp_32cf_t>, radar::rxMetaData>>
//       parsedBuffers_t;

//   void SignalInt();
//   void SystemMonitor();
//   void Log();

//   std::string makeProtoMsg(const radar::rxMetaData &md,
//                            const std::vector<float> &fftMag,
//                            const std::vector<float> &thresh,
//                            const std::vector<radar::target> &targets);

//   parsedBuffers_t ParseSweep(uint8_t *iq);

//   // signal processing classes
//   std::unique_ptr<FFT> mFftProc;
//   std::unique_ptr<cfar> mCfarProc;
//   std::unique_ptr<std::deque<parsedBuffers_t>> mQueue;
//   std::unique_ptr<std::deque<std::vector<radar::target>>> mDetQueue;
//   std::shared_ptr<spdlog::logger> console;
//   std::shared_ptr<configParser> config;

//   // threads
//   std::thread mDetThread, mMonitorThread, mLogThread;
//   std::mutex mBuffMutex;
//   std::condition_variable mWaitForBuff;

//   // variables
//   std::vector<radar::sp_32cf_t> mComplexIqBuffs;
//   std::vector<radar::sp_32cf_t *> mFftBuffs;
//   parsedBuffers_t parsedBuffers;

//   bool mBuffRdy, mEnabled, mWaiting, mSweepStarted;

//   int mProcNum, mBuffLen, mNumBuffs, mNumDets;
//   uint64_t mStartFreq, mEndFreq;
//   radar::sp_32cf_t *swapBuff;
//   std::vector<float> fftMag;

//   static constexpr int BlocksPerTransfer = 16;
// };
// } // namespace hackrf
