// /*
//  *
//  *
//  * Handles the rx and tx scheduling, also passes rx buffers to the processing
//  * thread
//  *
//  *
//  */

// #include <chrono>
// #include <cstring>
// #include <iostream>
// #include <scheduler.h>
// #include <stdio.h>
// #include <unistd.h>

// using namespace hackrf;

// // forward declaration of static members
// uint8_t *scheduler::rx_buff;
// uint32_t scheduler::numRxSamps;
// bool scheduler::ready;
// bool scheduler::retuned;
// bool scheduler::enabled;
// std::unique_ptr<processor> scheduler::pro;

// scheduler::scheduler(std::shared_ptr<configParser> config) : config(config)
// {
//     console = spdlog::stdout_color_mt("scheduler");
//     pro.reset(new processor(config));
// }

// scheduler::~scheduler()
// {
//     // check threads have joined
//     if (enabled)
//         this->stop();

//     console->info("Stopped scheduler");
// }

// void scheduler::findDevices()
// {
//     // enable and check the hardware
//     int result = hackrf_init();
//     if (result != HACKRF_SUCCESS)
//     {
//         console->error("No device found...stopping...");
//         std::exit(-1);
//     }
//     else
//     {
//         console->info("Found hackrf");
//     }

//     console->info("Initializing hardware");

//     result = hackrf_open(&hackrf);
//     if (result != HACKRF_SUCCESS)
//     {
//         console->error("Failed to open the device...stopping...");
//         std::exit(-1);
//     }
//     else
//     {
//         console->info("Opened hackrf");
//     }
// }

// // init hardware
// void scheduler::init()
// {
//     // set up device front end
//     int result = set_up_device(config, hackrf);
//     if (result == -1)
//     {
//         console->error("Device set up failed");
//         std::exit(-1);
//     }

//     console->info("Return from device setup: {}", result);
//     console->info("Starting Receiver");
//     sleep(1);

//     // get number of samples
//     uint32_t num_samples = 0;
//     int numSamps = (int)(config->getConfig<double>("radio", "sample_rate") *
//                          config->getConfig<double>("scanner", "dwell_time"));
//     if (numSamps < sampleMultiple)
//     {
//         console->warn(
//             "Requested {} samples, but the minimum number of samples is {}",
//             numSamps, 8192);
//         num_samples = sampleMultiple;
//     }
//     else if ((numSamps) % sampleMultiple)
//     {
//         num_samples = sampleMultiple *
//                       (int)std::ceil((float)numSamps / (float)sampleMultiple);
//         console->warn(
//             "Number of samples must be a multiple of {} \n\t using samples: {}",
//             8192, num_samples);
//     }

//     sleep(1);

//     // start the receiver
//     int ret = hackrf_start_rx(hackrf, &scheduler::rx_callback, (void *)this);
//     if (ret != HACKRF_SUCCESS)
//     {
//         console->error("Failed to start rx with error code: {}....stopping", ret);
//         hackrf_exit();
//         std::exit(-1);
//     };

//     // if the scanner is enabled, build a list of tune bands from the starting
//     // frequency to the end spaced apart by the sample rate
//     if (config->getConfig<bool>("scanner", "enabled"))
//     {
//         // get frequency ranges
//         double startFreq = config->getConfig<double>("scanner", "start_frequency");
//         double endFreq = config->getConfig<double>("scanner", "stop_frequency");
//         double bandwidth = config->getConfig<double>("scanner", "bandwidth");
//         double step_size = (startFreq) / 1e6;
//         int num_ranges = config->getConfig<int>("scanner", "num_bands");

//         if (num_ranges > 10)
//         {
//             console->warn("Requested {} tune bands, max number is 10", num_ranges);
//             num_ranges = 10;
//         }
//         else
//         {
//             console->info("Using {} bands", num_ranges);
//         };

//         console->info("Configuring {} bands: starting freqency: {} bandwidth: {}",
//                       num_ranges, startFreq, bandwidth);

//         uint16_t frequencies[2 * 10];

//         frequencies[0] = (uint16_t)(startFreq / 1e6); // start
//         frequencies[1] = (uint16_t)(endFreq / 1e6);   // end

//         for (size_t i = 0; i < (size_t)num_ranges; ++i)
//         {
//             // make sure start and stop are seperated by a multiple of the bandwidth
//             int step_count =
//                 1 + (frequencies[2 * i + 1] - frequencies[2 * i] - 1) / 20;
//             frequencies[2 * i + 1] =
//                 frequencies[2 * i] +
//                 step_count * 20; /// TODO: don't hardcode the step size
//         }

//         // program the sweep
//         result = hackrf_init_sweep(this->hackrf, frequencies, num_ranges,
//                                    num_samples * 2, (uint32_t)bandwidth, 7500000,
//                                    INTERLEAVED);
//         console->info("Return from sweep init: {}", result);

//         // set up processor
//         console->info("Starting processor");
//         pro->Init(frequencies[0], frequencies[2 * num_ranges - 1]);
//     }
//     else
//     {
//         // set up processor
//         console->info("Starting processor");
//         uint16_t cf = (uint16_t)(
//             config->getConfig<uint64_t>("radio", "center_frequency") / 1000000);
//         pro->Init(cf, cf);
//     }

//     // set ready to false, wait for start
//     ready = false;
// } // init

// // start hardware
// void scheduler::start()
// {
//     // init flow controls
//     console->info("::STARTED SPECTRUM MONITOR::\n");
//     enabled = true;
// }

// // stop hardware
// void scheduler::stop()
// {
//     enabled = false;

//     pro->Stop();

//     // stop hackrf device
//     hackrf_close(hackrf);
//     hackrf_exit();
// }

// //---------------------Receiver-------------------------
// int scheduler::rx_callback(hackrf_transfer *transfer)
// {
//     if (enabled)
//     {
//         // receive
//         rx_buff = transfer->buffer;

//         // give the buffer to the processor
//         pro->RxMonitor(rx_buff);
//     }
//     return 0;
// };
