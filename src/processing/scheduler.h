// #pragma once

// #include <atomic>
// #include <condition_variable>
// #include <mutex>
// #include <thread>

// #include <processor.h>
// #include <driver/hackrf.h>
// #include <driver/device_setup.h>
// #include <RadarTypes.h>
// #include <spdlog/sinks/stdout_color_sinks.h>
// #include <configParser.h>

// //TODO: implement some kind of common time base, could use the system time but it seems bad...
// namespace hackrf
// {
// class scheduler
// {
// public:
//   scheduler(std::shared_ptr<configParser> config);
//   ~scheduler();
//   void init();        //init hardware
//   void start();       //start threads
//   void stop();        //stop threads
//   void findDevices(); //find all sdrs attached to the computer

// private:
//   //private methods
//   void rx_callback_control(); //handle rx
//   static int rx_callback(hackrf_transfer *transfer);

//   //hackrf variables
//   hackrf_device *hackrf; //device pointer

//   //buffers and things...
//   static uint8_t *rx_buff;
//   static uint32_t numRxSamps;

//   uint32_t numTotalSamps;
//   int_fast32_t band;
//   int_fast32_t buff;
//   int_fast64_t numSamps;
//   static std::unique_ptr<processor> pro;

//   std::shared_ptr<spdlog::logger> console;
//   std::shared_ptr<configParser> config;

//   //thread controls
//   static bool enabled;
//   static bool ready;
//   static bool retuned;

//   static constexpr int sampleMultiple = 8192;
// };
// } // namespace hackrf
