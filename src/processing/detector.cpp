#include <processing/detector.hpp>
#include <fstream>

#include <signal_processing/spec_est/psd.hpp>
#include <signal_processing/signal_utils.hpp>
#include <iostream>
#include <spdlog/sinks/stdout_color_sinks.h>

detector::detector(spec::config_t config_) : config(config_)
{
    auto console = spdlog::stdout_color_mt("DETECTOR");
    std::string window = config->getConfig<std::string>("DETECTOR", "WINDOW");
    unsigned int fftsize = config->getConfig<unsigned int>("DETECTOR", "FFT_SIZE");
    sample_rate = config->getConfig<unsigned int>("RADIO", "SAMPLE_RATE");
    doBlanker = config->getConfig<bool>("DETECTOR", "BLANKER");
    blankerIdxs = std::vector<std::size_t>{0, 1, fftsize - 1, fftsize - 2};

    double pfa = config->getConfig<double>("DETECTOR", "PFA");
    int num_avg_bins = config->getConfig<int>("DETECTOR", "NUM_AVG_BINS");
    int num_guard_bins = config->getConfig<int>("DETECTOR", "NUM_GUARD_BINS");

    SPDLOG_LOGGER_INFO(console, "What is taking so long to start up???");
    SPDLOG_LOGGER_INFO(console, "FFT???");
    fftp = std::make_shared<FFT>(fftsize, window); //TODO: why is this so slow??????????????????? Its the FFT plan
    SPDLOG_LOGGER_INFO(console, "FFT DONE");
    SPDLOG_LOGGER_INFO(console, "CFAR???");
    detMaster = std::make_unique<cfar>(pfa, num_avg_bins, num_guard_bins, (int)fftsize, 0, sample_rate);
    SPDLOG_LOGGER_INFO(console, "CFAR DONE");

    SPDLOG_LOGGER_INFO(console, "NET1???");
    pub = std::make_unique<net::Publisher>("detections", 50000);
    SPDLOG_LOGGER_INFO(console, "NET2???");
    psd_pub = std::make_unique<net::Publisher>("psd", 50001);
    SPDLOG_LOGGER_INFO(console, "NETS DONE");
}

detector::~detector() {}

void detector::process(const uint64_t freq, const int64_t time, spec::AlignedVector<spec::fc32_t> data)
{
    spec::AlignedVector<float> psd;

    // auto epoch_time = time.time_since_epoch().count();

    std::cout << freq << ", " << time << ", " << data[0] << std::endl;

    // estimtate PSD
    welches_psd(psd, data, fftp);
    spec_dsp::blanker(psd, blankerIdxs); // blank out spurs

    spec_dsp::fft_shift(psd);

    auto start = std::chrono::steady_clock::now();
    auto out = detMaster->getDetections(psd);
    auto end = std::chrono::steady_clock::now();
    double proc_time = std::chrono::duration_cast<std::chrono::duration<double>>(end-start).count();

    for (const auto &det : out.first)
    {
        auto d = detection();
        d.set_msgname("DETECTION");
        d.set_cf_hz(freq + det.cf_hz);
        d.set_bw_hz(det.bw_hz);
        d.set_pwr_lin(det.pwr_lin);
        d.set_time(time);
        d.set_proc_time(proc_time);

        auto wrap = wrapper();
        wrap.set_msgtype(d.msgname());
        wrap.set_message(d.SerializeAsString());
        pub->send(wrap);
    }

    Meta* meta = new Meta();
    meta->set_time_ns(time);
    meta->set_host("detector");
    meta->set_sample_rate(sample_rate);
    meta->set_duration_s(0);
    meta->set_frequency_hz(freq);
    meta->set_bandwidth_hz(sample_rate);
    meta->set_complex(false);

    auto psd_msg = fft();
    psd_msg.set_msgname("FFT");
    psd_msg.set_allocated_meta(meta);
    psd_msg.set_comp(Compression::NONE);
    for(const auto& p : psd){
        psd_msg.add_data(p);
    }

    auto wrap = wrapper();
    wrap.set_msgtype(psd_msg.msgname());
    wrap.set_message(psd_msg.SerializeAsString());
    psd_pub->send(wrap);

    // auto psd = 

    // auto f = std::ofstream("psd.bin", std::ios::binary | std::ios::app);
    // f.write(reinterpret_cast<char *>(&epoch_time), sizeof(int64_t));
    // f.write(reinterpret_cast<char *>(&fftp->fftSize), sizeof(unsigned int));
    // f.write(reinterpret_cast<char *>(out.second.data()), sizeof(float) * psd.size());
    // f.write(reinterpret_cast<char *>(psd.data()), sizeof(float) * psd.size());
    // f.close();
}