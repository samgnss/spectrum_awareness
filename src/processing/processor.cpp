// #include "processor.h"

// #include <fstream>
// #include <functional>
// #include <iostream>
// #include <memory>
// #include <unistd.h>
// #include <volk/volk.h>

// #include <ctime>

// #include <MsgDefs.pb.h>

// using namespace hackrf;

// processor::processor(std::shared_ptr<configParser> config) : config(config)
// {
//   mNumDets = 0;
//   size_t alignment = volk_get_alignment();
//   swapBuff = (radar::sp_32cf_t *)volk_malloc(
//       BYTES_PER_BLOCK * sizeof(radar::sp_32cf_t), alignment);
//   fftMag = std::vector<float>();
//   mQueue.reset(new std::deque<parsedBuffers_t>());
//   console = spdlog::stdout_color_mt("processor");
// }

// processor::~processor()
// {
//   if (mEnabled)
//     Stop();

//   // clean up memory
//   volk_free(swapBuff);

//   console->info("Stopped processor");
// }

// void processor::Stop()
// {
//   mEnabled = false;

//   // stop the ZMQ server, if it is still running
//   console->info("Trying to stop the FFT server");
//   if (ZmqPushServer::getInstance().isRunning())
//     ZmqPushServer::getInstance().stopServer();

//   // force the processing threads to stop
//   mBuffRdy = true;
//   mWaitForBuff.notify_one();

//   // join threads
//   mDetThread.join();
//   mMonitorThread.join();
//   mLogThread.join();
// }

// void processor::Init(uint16_t startFreq, uint16_t endFreq)
// {
//   mStartFreq = startFreq * 1e6;
//   mEndFreq = endFreq * 1e6;

//   int fftSize = config->getConfig<int>("detector", "fft_size");
//   int numAvgBins = config->getConfig<int>("detector", "num_avg_bins");
//   int numGuardBins = config->getConfig<int>("detector", "num_guard_bins");
//   int freqSlip = config->getConfig<int>("detector", "freq_slip");
//   int sampleRate = config->getConfig<int>("scanner", "bandwidth");
//   float pfa = config->getConfig<float>("detector", "pfa");

//   fftMag.resize(fftSize);

//   // Thread controls
//   mEnabled = true;
//   mBuffRdy = false;
//   mWaiting = false;
//   mSweepStarted = false;

//   // fft object
//   mFftProc.reset(new FFT(fftSize, BYTES_PER_BLOCK / sizeof(radar::sp_32cf_t),
//                          config->getConfig<std::string>("detector", "window")));

//   // cfar detector
//   mCfarProc.reset(
//       new cfar(pfa, numAvgBins, numGuardBins, fftSize, freqSlip, sampleRate));

//   // bind threads
//   mDetThread = std::thread(std::bind(&processor::SignalInt, this));
//   // mMonitorThread = std::thread(std::bind(&processor::SystemMonitor, this));
//   // mLogThread = std::thread(std::bind(&processor::Log, this));

//   // make sure the server is running
//   ZmqPushServer &server = ZmqPushServer::getInstance();
//   if (!server.isRunning())
//   {
//     console->info("Starting FFT server");
//     server.runServer(5555);
//   }
// }

// void processor::RxMonitor(uint8_t *rx_buff)
// {
//   auto buffs = processor::ParseSweep(rx_buff);

//   // add the parsed buffers to the queue
//   std::unique_lock<std::mutex> lk(mBuffMutex);
//   mQueue->push_back(buffs);
// }

// void processor::SignalInt()
// {
//   while (mEnabled)
//   {
//     // lock mutex
//     std::unique_lock<std::mutex> lk(mBuffMutex);

//     if (mEnabled and mQueue->size() > 0)
//     {
//       auto buffToProcess = mQueue->front();
//       console->debug("Processing buffers");
//       for (auto &buff : buffToProcess)
//       {
//         mFftProc->getFFT_ABS(buff.first.data(), fftMag.data());
//         auto thresh = mCfarProc->getDetections(fftMag);

//         // serialize the data
//         auto msg =
//             makeProtoMsg(buff.second, fftMag, thresh.second, thresh.first);

//         // queue it at the server
//         ZmqPushServer::getInstance().queueData(msg);
//       }
//       mQueue->pop_front();
//     }
//     lk.unlock();
//   }
// }

// std::string processor::makeProtoMsg(const radar::rxMetaData &md,
//                                     const std::vector<float> &fftMag,
//                                     const std::vector<float> &thresh,
//                                     const std::vector<radar::target> &targets)
// {
//   // proto message type
//   spectrum::fftThreshCombo comboMsg;
//   comboMsg.set_freq_hz(md.freqHz);
//   comboMsg.set_time_ms(md.time);

//   int msgSize = fftMag.size();
//   for (int i = 0; i < msgSize; ++i)
//   {
//     comboMsg.add_fftmag(fftMag[i]);
//     comboMsg.add_thresh(thresh[i]);
//   }

//   for (int i = 0; i < targets.size(); ++i)
//   {
//     comboMsg.add_det();
//     auto det = comboMsg.mutable_det(i);
//     det->set_freq_hz(targets[i].freqHz.first);
//     det->set_bandwidth_hz(targets[i].freqHz.first);
//     det->set_time_us(targets[i].freqHz.first);
//     det->set_amp(targets[i].power);
//   }

//   return comboMsg.SerializeAsString();
// }

// void processor::SystemMonitor() {}

// void processor::Log() {}

// processor::parsedBuffers_t processor::ParseSweep(uint8_t *rx)
// {
//   uint64_t frequency;
//   processor::parsedBuffers_t parse;
//   radar::rxMetaData rxMd;

//   // loop through all bands
//   for (int band = 0; band < BlocksPerTransfer; ++band)
//   {
//     // get the frequency of the buffer
//     if (rx[0] == 0x7F && rx[1] == 0x7F)
//     {
//       frequency = ((uint64_t)(rx[9]) << 56) | ((uint64_t)(rx[8]) << 48) |
//                   ((uint64_t)(rx[7]) << 40) | ((uint64_t)(rx[6]) << 32) |
//                   ((uint64_t)(rx[5]) << 24) | ((uint64_t)(rx[4]) << 16) |
//                   ((uint64_t)(rx[3]) << 8) | rx[2];
//     }
//     else
//     {
//       rx += BYTES_PER_BLOCK;
//       console->debug("Checksum failed");
//       continue;
//     }

//     // check if we started the sweep
//     if (frequency == (uint64_t)(mStartFreq) && mSweepStarted == false)
//     {
//       mSweepStarted = true;
//     }

//     // check if we are currently in the middle of a stream; if we are, wait for
//     // it to start
//     if (!mSweepStarted)
//     {
//       rx += BYTES_PER_BLOCK;
//       console->warn("Waiting for stream to start at frequency ({}) vs start "
//                     "frequency ({})",
//                     frequency, mStartFreq);
//       continue;
//     }

//     // invalid frequency
//     if (frequency > mEndFreq || frequency < mStartFreq)
//     {
//       rx += BYTES_PER_BLOCK;
//       console->warn("Got invalid frequency: {}", frequency);
//       continue;
//     }

//     // we have a valid buffer!
//     std::pair<std::vector<radar::sp_32cf_t>, radar::rxMetaData> tmp;
//     parse.push_back(tmp);

//     rxMd.band = band;
//     rxMd.freqHz = frequency;
//     auto tNow = std::chrono::high_resolution_clock::now();
//     rxMd.time = (uint64_t)std::chrono::system_clock::to_time_t(tNow);
//     rxMd.valid = true;

//     math::interleavedUCharToComplexFloat(rx, swapBuff, BYTES_PER_BLOCK);

//     parse.back().second = std::move(rxMd);
//     parse.back().first.resize(BYTES_PER_BLOCK / sizeof(radar::sp_32cf_t));

//     std::copy(&swapBuff[0],
//               &swapBuff[BYTES_PER_BLOCK / sizeof(radar::sp_32cf_t)],
//               parse.back().first.begin());

//     rx += BYTES_PER_BLOCK;
//     console->debug("Parsed buffer");
//   }

//   mSweepStarted = false;

//   return parse;
// }