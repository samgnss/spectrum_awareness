#pragma once

#include <chrono>
#include <thread>
#include <mutex>
#include <deque>

#include <common/common.hpp>
#include <network/publisher.hpp>
#include <signal_processing/spec_est/fft.h>
#include <signal_processing/spec_est/psd.hpp>
#include <signal_processing/cfar.h>

class detector
{
public:
    detector(spec::config_t config_);
    ~detector();

    void start();
    void stop();

    void process(const uint64_t freq, const int64_t time, spec::AlignedVector<spec::fc32_t> data);

private:
    spec::config_t config;
    std::shared_ptr<FFT> fftp;
    std::unique_ptr<cfar> detMaster;
    std::thread channelThread;
    std::mutex queueMut;
    std::deque<std::vector<spec::detection>> detQueue;
    std::unique_ptr<net::Publisher> pub;
    std::unique_ptr<net::Publisher> psd_pub;

    unsigned int sample_rate;
    bool doBlanker;
    std::vector<std::size_t> blankerIdxs;
    

    void channelWorker(double freqSlip, int numHitsBeforeDetect);
};