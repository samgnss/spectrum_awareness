add_library(spectrum_common common/common.hpp
                            common/ConfigParser.hpp)

target_link_libraries(spectrum_common PUBLIC
                                   spdlog::spdlog
                                   ${YAML_CPP_LIBRARIES}
                     )
target_include_directories(spectrum_common PUBLIC ./)            
set_target_properties(spectrum_common PROPERTIES LINKER_LANGUAGE CXX)

add_library(spectrum_dsp signal_processing/signal_utils.hpp
                                signal_processing/cfar.h
                                signal_processing/cfar.cpp
                                signal_processing/spec_est/fft.h
                                signal_processing/spec_est/fft.cpp
                                signal_processing/spec_est/psd.hpp
                                signal_processing/spec_est/psd.cpp
                                signal_processing/spec_est/stft.h
                                signal_processing/spec_est/stft.cpp
                                signal_processing/waveforms/cw.hpp)

target_link_libraries(spectrum_dsp PUBLIC
                     spectrum_common
                     PRIVATE
                     -lfftw3f
                     -lvolk
                     )
target_include_directories(spectrum_dsp PUBLIC ./)

file(GLOB SPEC_PROTO_SRC network/*.pb*)
add_library(spectrum_networking network/publisher.hpp
                                network/publisher.cpp
                                ${SPEC_PROTO_SRC})

target_link_libraries(spectrum_networking PUBLIC
                        spectrum_common
                        ${ZeroMQ_LIBRARY}
                        ${PROTOBUF_LIBRARIES}                  
)
target_include_directories(spectrum_networking PUBLIC ./)

add_library(spectrum_processing processing/detector.hpp
                                processing/detector.cpp)

target_link_libraries(spectrum_processing PUBLIC
                        spectrum_common
                        spectrum_dsp
                        spectrum_networking
       )
target_include_directories(spectrum_processing PUBLIC ./)

add_library(spectrum_hackrf hardware/hackrf/device_setup.h
                            hardware/hackrf/HackRfIf.hpp
                            hardware/hackrf/HackRfIf.cpp)

target_link_libraries(spectrum_hackrf PUBLIC
                            spectrum_common
                            ${LIBUSB_LIBRARIES}
                            ${LIBHACKRF_LIBRARIES}
                     )
target_include_directories(spectrum_hackrf PUBLIC ./)

add_library(spectrum_soapy hardware/soapy/SoapyIf.hpp
                           hardware/soapy/SoapyIf.cpp)

target_link_libraries(spectrum_soapy PUBLIC
                     spectrum_common
                     ${SOAPYSDR_LIBRARIES}
              )
target_include_directories(spectrum_soapy PUBLIC ./ /opt/include) #damn

