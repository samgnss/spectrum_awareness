#include <atomic>
#include <thread>
#include <mutex>
#include <common/common.hpp>

extern "C"
{
#include <libhackrf/hackrf.h>
}

namespace radio
{
class HackRfIf
{
public:
    HackRfIf(spec::config_t config, spec::console_t console);

    void start();
    void stop();

    void registerCallback(spec::RXCallback_t callback_);

private:
    spec::config_t config;
    spec::console_t console;
    std::atomic_bool running;
    hackrf_device *device; //device pointer
    double rate;
    std::thread procThread;

    static uint64_t freq;
    static int rxCallback(hackrf_transfer *transfer);
    static spec::RXCallback_t callback;

    void init();
    void processBuffer(uint8_t *buffer);
};
} // namespace radio