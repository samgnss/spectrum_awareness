#include "HackRfIf.hpp"

#include <signal_processing/signal_utils.hpp>

spec::RXCallback_t radio::HackRfIf::callback;
uint64_t radio::HackRfIf::freq;

radio::HackRfIf::HackRfIf(spec::config_t config, spec::console_t console) : config(config)
{
    console = spdlog::stdout_color_mt("HACKRFIF");
    init();
}

void radio::HackRfIf::init()
{
    int ret = hackrf_init();
    if (ret != HACKRF_SUCCESS)
    {
        console->error("Hackrf init failed with error code: {}", ret);
        throw std::exception();
    }

    ret = hackrf_open(&device);
    if (ret != HACKRF_SUCCESS)
    {
        console->error("No device found!");
        throw std::exception();
    }

    rate = config->getConfig<double>("RADIO", "SAMPLE_RATE");
    ret = hackrf_set_sample_rate(device, rate);

    auto analog_bw = hackrf_compute_baseband_filter_bw(config->getConfig<uint32_t>("RADIO", "BANDWIDTH"));
    ret |= hackrf_set_baseband_filter_bandwidth(device, analog_bw);

    ret |= hackrf_set_vga_gain(device, config->getConfig<uint32_t>("RADIO", "VGA_GAIN"));

    if (ret != HACKRF_SUCCESS)
    {
        console->error("Error setting up front end!");
        throw std::exception();
    }

    // set up scanning
    uint32_t num_samples = 0;
    int numSamps = (int)(rate * config->getConfig<double>("RADIO", "DWELL_TIME_US") / 1e6);
    if (numSamps < 8192)
    {
        console->warn("Requested {} samples, but the minimum number of samples is {}", numSamps, 8192);
        num_samples = 8192;
    }
    else if (!(numSamps % 8192))
    {
        num_samples = 8192 * (int)std::ceil((float)numSamps / (float)8192);
        console->warn("Number of samples must be a multiple of {} \n\t using samples: {}", 8192, num_samples);
    }

    // get frequency ranges
    double startFreq = config->getConfig<double>("SCANNER", "START");
    double endFreq = config->getConfig<double>("SCANNER", "STOP");
    double bandwidth = config->getConfig<double>("SCANNER", "BANDWIDTH");
    double step_size = (bandwidth) / 1e6;
    int num_bands = config->getConfig<int>("SCANNER", "NUM_BANDS");

    if (num_bands > 10)
    {
        console->warn("Requested {} tune bands, max number is 10", num_bands);
        num_bands = 10;
    }
    else
    {
        console->info("Using {} bands", num_bands);
    };

    console->info("Configuring {} bands: starting freqency: {} bandwidth: {}", num_bands, startFreq, bandwidth);

    uint16_t frequencies[2 * num_bands];

    frequencies[0] = (uint16_t)(startFreq / 1e6); // start
    frequencies[1] = (uint16_t)(endFreq / 1e6);   // end

    for (size_t i = 0; i < (size_t)num_bands; ++i)
    {
        // make sure start and stop are seperated by a multiple of the bandwidth
        int step_count = 1 + (frequencies[2 * i + 1] - frequencies[2 * i] - 1) / 20;
        frequencies[2 * i + 1] = frequencies[2 * i] + step_count * 20; /// TODO: don't hardcode the step size
    }

    // set frequency
    freq = config->getConfig<uint64_t>("RADIO", "CENTER_FREQUENCY");
    ret = hackrf_set_freq(device, freq);
    if (ret != HACKRF_SUCCESS)
    {
        console->error("Failed to set frequency with error code: {}....stopping", ret);
        hackrf_exit();
        std::exit(-1);
    };

    // // program the sweep
    // int result = hackrf_init_sweep(device, frequencies, num_bands,
    //                                num_samples * 2, (uint32_t)bandwidth, 7500000,
    //                                INTERLEAVED);
    // console->info("Return from sweep init: {}", result);
}

void radio::HackRfIf::start()
{
    // start the receiver
    auto ret = hackrf_start_rx(device, &radio::HackRfIf::rxCallback, (void *)this);
    if (ret != HACKRF_SUCCESS)
    {
        console->error("Failed to start rx with error code: {}....stopping", ret);
        hackrf_exit();
        std::exit(-1);
    };
}

void radio::HackRfIf::registerCallback(spec::RXCallback_t callback_)
{
    callback = callback_;
}

int radio::HackRfIf::rxCallback(hackrf_transfer *transfer)
{
    auto tpoint = std::chrono::steady_clock::now();

    // convert from uint8 samples to complex float...
    auto vec = spec::AlignedVector<spec::fc32_t>(transfer->valid_length);
    spec_dsp::uint8_to_fc32(vec.data(), transfer->buffer, transfer->valid_length);

    callback(freq, (int64_t)0, vec);

    return 0;
}