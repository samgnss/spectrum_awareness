// #pragma once

// #include <iostream>

// #include "hackrf.h"
// #include <RadarTypes.h>
// #include <configParser.h>

// namespace hackrf
// {
// inline int set_up_device(std::shared_ptr<configParser> config, hackrf_device *device)
// {
//     //set sample rate
//     int ret = hackrf_set_sample_rate_manual(device, config->getConfig<uint32_t>("radio", "sample_rate"), 1);
//     if (ret != HACKRF_SUCCESS)
//     {
//         std::cout << "Failed to set sample rate with error code: " << hackrf_error(ret) << std::endl;
//         return -1;
//     }
//     //set filter bandwidth
//     ret = hackrf_set_baseband_filter_bandwidth(device, config->getConfig<uint32_t>("radio", "bandwidth"));
//     if (ret != HACKRF_SUCCESS)
//     {
//         std::cout << "Failed to set filter bandwidth with error code: " << hackrf_error(ret) << std::endl;
//         return -1;
//     }

//     //set rx gains
//     ret = hackrf_set_vga_gain(device, config->getConfig<uint32_t>("radio", "lna_gain"));
//     ret |= hackrf_set_lna_gain(device, config->getConfig<uint32_t>("radio", "vga_gain"));
//     ret |= hackrf_set_rf_gain(device, 0);
//     if (ret != HACKRF_SUCCESS)
//     {
//         std::cout << "Failed to set front end gain with error code: " << hackrf_error(ret) << std::endl;
//         return -1;
//     }

//     return 0;
// };
// }; // namespace hackrf
