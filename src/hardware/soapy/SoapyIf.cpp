#include "SoapyIf.hpp"
#include <ctime>

radio::SoapyIf::SoapyIf(spec::config_t config, spec::console_t console)
    : config(config), console(console) {
  // make the soapy device, it will use whatever device it finds first
  device = SoapySDR::Device::make();

  device->listAntennas(0, 0);
  auto rates = device->getSampleRateRange(SOAPY_SDR_RX, 0);
  auto freqs = device->getFrequencyRange(SOAPY_SDR_RX, 0);

  sample_rate = config->getConfig<double>("RADIO", "SAMPLE_RATE");

  // set sample rate
  SPDLOG_LOGGER_INFO(console, "Setting RX rate: {} Msps", sample_rate / 1e6);
  device->setSampleRate(SOAPY_SDR_RX, 0, sample_rate);
  const double actualRXRate = device->getSampleRate(SOAPY_SDR_RX, 0);
  SPDLOG_LOGGER_INFO(console, "Actual RX rate: {} Msps", actualRXRate / 1e6);

  SPDLOG_LOGGER_INFO(console, "Making tune steps");
  double bandwidth = config->getConfig<double>("RADIO", "BANDWIDTH");
  std::vector<double> ranges;
  config->getSequence<double>("RADIO", "TUNE_RANGES", ranges);
  double dwell_time = config->getConfig<double>("RADIO", "DWELL_TIME");
  bool closed = config->getConfig<bool>("RADIO", "CLOSED");
  SPDLOG_LOGGER_INFO(console, "TUNE STEP SIZE {}", ranges.size());
  if (ranges.size() == 1){
    BuildTuneSteps(bandwidth, ranges[0], ranges[0], dwell_time, false);
  } 
  else{
    for(unsigned int i = 0; i < ranges.size(); i+=2){
      BuildTuneSteps(bandwidth, ranges[i], ranges[i+1], dwell_time, closed);
    }
  }
}

radio::SoapyIf::~SoapyIf(){}

void radio::SoapyIf::Start()
{
  running = true;

  console->info("Starting processing thread");
  procThread = std::thread(std::bind(&radio::SoapyIf::Sweep, this));
}

void radio::SoapyIf::Stop()
{
  running = false;
  procThread.join();

  if (device != nullptr)
    {
        console->info("Closing device");
        SoapySDR::Device::unmake(device);
    }

}

void radio::SoapyIf::RegisterCallback(spec::RXCallback_t callback_){
  callback = callback_;
}


void radio::SoapyIf::Sweep()
{
  SPDLOG_LOGGER_INFO(console, "Starting sweep");
  // setup buffers
  spec::AlignedVector<spec::fc32_t> buffer;
  std::vector<void *> rxBuffs(1);
  rxBuffs[0] = buffer.data();

  SoapySDR::Stream *rxStream;
  rxStream = device->setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, {0});

  // TODO: use a multiple of the MTU for streaming
  auto const streamMTU = device->getStreamMTU(rxStream);
  SPDLOG_LOGGER_DEBUG(console, "Stream MTU size: {} samples", streamMTU);

  while(running){
    for(const auto& tstep : tuneSteps){
      if (!running){
        break;
      }
      SPDLOG_LOGGER_INFO(console, "Sweeping {} for {} seconds", tstep.frequencyHz, tstep.dwell_time_s);
      unsigned int num_samps = std::ceil(tstep.dwell_time_s*sample_rate);
      SPDLOG_LOGGER_INFO(console, "Grabbing {} samples", num_samps);
      if(buffer.size() < num_samps){
        SPDLOG_LOGGER_INFO(console, "Resizing buffer to {}", num_samps);
        buffer.resize(num_samps);
        rxBuffs[0] = buffer.data();
        device->activateStream(rxStream);
      }
      device->setFrequency(SOAPY_SDR_RX, 0, tstep.frequencyHz);
      // device->setFrequency(SOAPY_SDR_RX, 0, tstep.frequencyHz);
      // std::this_thread::sleep_for(std::chrono::microseconds(100));
      // device->activateStream(rxStream);
      int flags = 0;
      int64_t timeNs;
      int ret = device->readStream(rxStream, rxBuffs.data(), num_samps, flags, timeNs);
      // device->deactivateStream(rxStream);
      auto utctime = std::chrono::system_clock::now().time_since_epoch();
      timeNs = std::chrono::duration_cast<std::chrono::nanoseconds>(utctime).count();
      //std::cout << buffer[0] << std::endl;
      // do processing
      callback(tstep.frequencyHz, timeNs, buffer);
    }
  }
  device->deactivateStream(rxStream);
  device->closeStream(rxStream);
  SPDLOG_LOGGER_INFO(console, "Sweep stopped");
}


void radio::SoapyIf::CleanUp(){}

void radio::SoapyIf::BuildTuneSteps(const double bandwidth, const double start_freq, const double stop_freq, const double dwell_time, const bool closed){
   // we will tune to half the bandwidth
   double step_size = bandwidth/2.0f;
   
   double tune_freq = start_freq;
   while(tune_freq <= stop_freq){
     tuneSteps.emplace_back(tune_freq, dwell_time, "TODO");
     tune_freq += step_size;
   }

   if (closed && tuneSteps.back().frequencyHz != stop_freq){
     tuneSteps.emplace_back(stop_freq, dwell_time, "TODO");
   }
}


