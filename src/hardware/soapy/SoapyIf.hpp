#pragma once

#include <atomic>
#include <thread>
#include <vector>

#include <common/common.hpp>

#include <SoapySDR/Device.hpp>
#include <SoapySDR/Formats.hpp>
#include <SoapySDR/Time.hpp>

namespace radio {

struct TuneStep {
  TuneStep(uint64_t freq, double dwell, std::string antenna)
      : frequencyHz(freq), dwell_time_s(dwell), antenna(antenna){};
  TuneStep() = delete;
  ~TuneStep() = default;
  uint64_t frequencyHz;
  double dwell_time_s;
  std::string antenna;
};

class SoapyIf {
public:
  /**
   * @brief Construct a new SoapyIf object
   *
   * @param config
   * @param console
   */
  SoapyIf(spec::config_t config, spec::console_t console);

  /**
   * @brief Destroy the Soapy If object
   *
   */
  ~SoapyIf();

  /**
   * @brief Start all the things
   *
   */
  void Start();

  /**
   * @brief Stop all the things
   *
   */
  void Stop();

  /**
   * @brief Add RX callback
   *
   * @param callback_
   */
  void RegisterCallback(spec::RXCallback_t callback_);

  std::atomic_bool running = false;
private:
  const spec::config_t config;
  const spec::console_t console;
  double sample_rate;

  SoapySDR::Device *device;

  std::thread procThread;
  spec::RXCallback_t callback;

  std::vector<TuneStep> tuneSteps;

  /**
   * @brief Sweep all the things
   *
   */
  void Sweep();

  /**
  * @brief Transmit the sweep
  */
  void TxSweep();

  /**
   * @brief Clean up stuff
   *
   */
  void CleanUp();


  void BuildTuneSteps(const double bandwidth, const double start_freq, const double stop_freq, const double dwell_time, const bool closed=false);

};
} // namespace radio