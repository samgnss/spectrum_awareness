#include <network/publisher.hpp>

net::Publisher::Publisher(const std::string &topic, const int port) : topic(topic), port(port)
{
    context = zmq::context_t(1);
    socket = std::make_unique<zmq::socket_t>(context, ZMQ_PUB);
    socket->bind("tcp://127.0.0.1:" + std::to_string(port));
};

net::Publisher::~Publisher()
{
    socket->close();
};

void net::Publisher::send(wrapper msg) const
{
    const auto serMsg = msg.SerializeAsString();
    const auto fullMsg = topic + " " + serMsg;

    zmq::message_t zmqMsg(fullMsg.size());
    std::memcpy(zmqMsg.data(), fullMsg.c_str(), fullMsg.size());
    socket->send(zmqMsg, ZMQ_NOBLOCK);
};