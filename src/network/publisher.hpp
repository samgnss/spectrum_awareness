#pragma once
#include <common/common.hpp>
#include <zmq.hpp>
#include <network/common.pb.h>

namespace net
{
class Publisher
{
public:
    Publisher(const std::string &topic, const int port);
    ~Publisher();
    void send(wrapper msg) const;

private:
    const std::string topic;
    const int port;
    zmq::context_t context;
    std::unique_ptr<zmq::socket_t> socket;
};
} // namespace net