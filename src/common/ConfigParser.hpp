#pragma once

#include <iostream>
#include <mutex>
#include <string>
#include <vector>
#include <yaml-cpp/yaml.h>

class ConfigParser {
public:
  ConfigParser(const std::string configFileName);

  template <typename T>
  T getConfig(const std::string sectionName, const std::string varName);

  template <typename T>
  T getConfig(const std::string sectionName, const std::string varName, const T defaultVal);

  template <typename T>
  void getSequence(const std::string sectionName, const std::string varName, std::vector<T>& ret);

private:
  std::mutex readCtl;
  YAML::Node config;
};

inline ConfigParser::ConfigParser(const std::string configFileName) {
  config = YAML::LoadFile(configFileName);
}

template <typename T>
T ConfigParser::getConfig(const std::string sectionName, const std::string varName) {
  T item;
  std::lock_guard<std::mutex> lk(readCtl);
  try {
    item = std::move(config[sectionName][varName].as<T>());
  } catch (const YAML::Exception& e) {
    std::cerr << "Failed to parse config. Section name: " << sectionName
              << ", var name: " << varName << std::endl;
    std::cerr << e.what();
    throw e;
  }
  return item;
}

template <typename T>
T ConfigParser::getConfig(const std::string sectionName, const std::string varName,
                          const T defaultVal) {
  T item;
  std::lock_guard<std::mutex> lk(readCtl);
  try {
    item = std::move(config[sectionName][varName].as<T>());
  } catch (const YAML::Exception& e) {
    item = defaultVal;
  }
  return item;
}

template <typename T>
void ConfigParser::getSequence(const std::string sectionName,
                                         const std::string varName,
                                         std::vector<T>& ret) {
  bool isSequence;
  // std::vector<T> ret;
  try {
    isSequence = config[sectionName][varName].IsSequence();
  } catch (const YAML::Exception& e) {
    std::cerr << "Failed to parse config. Section name: " << sectionName
              << ", var name: " << varName << std::endl;
    std::cerr << e.what();
    throw e;
  }

  if (!isSequence) {
    return;
  }

  for (auto seq = config[sectionName][varName].begin(); seq != config[sectionName][varName].end(); ++seq) {
    std::cout << seq->as<T>() << std::endl;
    ret.push_back( seq->as<T>() );
  }
}
