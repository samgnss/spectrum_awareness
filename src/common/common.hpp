#pragma once
#include <complex>
#include <functional>
#include <memory>
#include <vector>
#include <chrono>
#include <volk/volk.h>

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <network/common.pb.h>
#include <network/detection.pb.h> //WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Why do i do things I do
#include <network/data.pb.h>

#include "ConfigParser.hpp"

namespace spec
{

/**
 * @brief AlignedAllocator adapted from: https://en.cppreference.com/w/cpp/named_req/Allocator
 * 
 * @tparam T 
 */
template <class T>
struct AlignedAllocator
{
    typedef T value_type;
    AlignedAllocator() = default;
    template <class U>
    constexpr AlignedAllocator(const AlignedAllocator<U> &) noexcept {}
        [[nodiscard]] T *allocate(std::size_t n)
    {
        if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
            throw std::bad_alloc();
        auto alignment = volk_get_alignment();
        if (auto p = static_cast<T *>(volk_malloc(n * sizeof(T), alignment)))
            return p;
        throw std::bad_alloc();
    }
    void deallocate(T *p, std::size_t) noexcept { volk_free(p); }
};
template <class T, class U>
bool operator==(const AlignedAllocator<T> &, const AlignedAllocator<U> &) { return true; }
template <class T, class U>
bool operator!=(const AlignedAllocator<T> &, const AlignedAllocator<U> &) { return false; }

using config_t = std::shared_ptr<ConfigParser>;
using fc32_t = std::complex<float>;
using sc16_t = std::complex<int16_t>;
using console_t = std::shared_ptr<spdlog::logger>;

template <class T>
using AlignedVector = std::vector<T, AlignedAllocator<T>>;
using RXCallback_t = std::function<void(const uint64_t, const int64_t, AlignedVector<fc32_t>)>;

// time
struct time
{
    time() = delete;
    time(double us, double ns) : us(us), ns(ns){};
    time(const time &t) : us(t.us), ns(t.ns){};
    ~time() = default;
    double us;
    double ns;
};

// RF detection
struct detection
{
    detection() = delete;
    detection(double cf_hz, double bw_hz, double pwr_lin, const time &t) : cf_hz(cf_hz),
                                                                           bw_hz(bw_hz),
                                                                           pwr_lin(pwr_lin),
                                                                           detTime(t){};
    detection(const detection &det) : cf_hz(det.cf_hz),
                                      bw_hz(det.bw_hz),
                                      pwr_lin(det.pwr_lin),
                                      detTime(det.detTime){};

    detection(const detection &&det) : cf_hz(det.cf_hz),
                                       bw_hz(det.bw_hz),
                                       pwr_lin(det.pwr_lin),
                                       detTime(det.detTime){};

    ~detection() = default;

    double cf_hz;
    double bw_hz;
    double pwr_lin;
    time detTime;
};

// Channel
struct channel
{
    channel();
    ~channel() = default;

    channel(double cf_hz, double bw_hz, time lastUpdate) : lastUpdate(lastUpdate), activeTime(lastUpdate)
    {
        active = false;
        count = 1;
        this->cf_hz = cf_hz;
        this->bw_hz = bw_hz;
    };

    bool overlaps(const spec::channel &chan) const
    {
        // check for overlap
        auto low = cf_hz - bw_hz / 2.0;
        auto high = cf_hz + bw_hz / 2.0;

        auto otherLow = chan.cf_hz - chan.bw_hz / 2.0;
        auto otherHigh = chan.cf_hz + chan.bw_hz / 2.0;

        return (low < otherHigh) && (otherLow < high);
    };

    void add(const spec::channel &chan)
    {
        auto low = cf_hz - bw_hz / 2.0;
        auto high = cf_hz + bw_hz / 2.0;

        auto otherLow = chan.cf_hz - chan.bw_hz / 2.0;
        auto otherHigh = chan.cf_hz + chan.bw_hz / 2.0;

        auto newHigh = std::max(high, otherHigh);
        auto newLow = std::min(low, otherLow);

        this->cf_hz = (newHigh + newLow) / 2.0;
        this->count += chan.active;
    };

    bool active;
    uint64_t count;
    double cf_hz;
    double bw_hz;

    time lastUpdate;
    time activeTime;
};

}; // namespace spec